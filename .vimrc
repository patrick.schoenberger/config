set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'valloric/youcompleteme'

Plugin 'jdonaldson/vaxe'

call vundle#end()
filetype plugin indent on

set number
set relativenumber
set tabstop=2
syntax on

map <F5> :w<CR> :!make run<CR>
map <F6> :w<CR> :!bear make<CR>:YcmRestartServer<CR>

let g:ycm_autoclose_preview_window_after_insertion = 1
