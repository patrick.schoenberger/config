set -x PATH $PATH ~/.scripts ~/.bin ~/.bin/*

set LD_LIBRARY_PATH (string join ":" $HOME/.lib/*)
export LD_LIBRARY_PATH
export LIBRARY_PATH=$LD_LIBRARY_PATH

set CPATH (string join ":" $HOME/.include/*)
export CPATH

if test -z (pgrep -x i3)
	x
end
